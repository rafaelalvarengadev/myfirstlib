package myfirstlib

import (
	"github.com/pkg/errors"
	"gitlab.com/rafaelalvarengadev/myfirstlib/pkg/UserFactory"
)

func NewUser(userType string, name string , age int , salary float64) (UserFactory.User, error) {
	switch userType {
	case "DEVELOPER":
		return UserFactory.Developer{Name: name ,Age: age, Salary: salary} , nil
	case "DBA":
		return UserFactory.DBA{Name: name, Age: age, Salary: salary} , nil
	default:
		return nil , errors.New("value of user unknown")
	}
}

func Developer() UserFactory.Developer {
	return UserFactory.Developer{}
}

func DBA() UserFactory.DBA {
	return UserFactory.DBA{}
}
