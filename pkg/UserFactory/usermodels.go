package UserFactory

type User interface {
	UserName() string
	UserAge()	int
	UserSalary() float64
}

type Developer struct {
	Name string
	Age int
	Salary float64
}

func (d Developer) UserName() string {
	return d.Name
}

func (d Developer) UserAge() int {
	return d.Age
}

func (d Developer) UserSalary() float64 {
	return d.Salary + (d.Salary *  0.2)
}

type DBA struct {
	Name string
	Age int
	Salary float64
}

func (e DBA) UserName() string {
	return e.Name
}

func (e DBA) UserAge() int {
	return e.Age
}

func (e DBA) UserSalary() float64 {
	return e.Salary + (e.Salary *  0.3)
}






